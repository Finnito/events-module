<?php

return [
    // Events
    "name" => [
        "name" => "Event Name",
        "placeholder" => "Hanmer Trip, Ball, Quiz Night",
        "instructions" => "More specific name (if applicable) for the event type.",
    ],
    "slug" => [
        "name" => "Event Slug",
        "instructions" => "Automatically generated - don't change me!",
    ],
    "type" => [
        "name" => "Event Type",
        "instructions" => "Create a new Event Type entry if adding a new type of event.",
    ],
    "start_time" => [
        "name" => "Start Date & Time",
        "instructions" => "",
    ],
    "end_time" => [
        "name" => "End Date & Time",
        "instructions" => "",
    ],
    "banner" => [
        "name" => "Banner Image",
        "instructions" => "The large full-screen image displayed on the event page.",
    ],
    "logo" => [
        "name" => "Logo",
        "instructions" => "Smaller, probably 1:1 ratio logo.",
    ],
    "tickets" => [
        "name" => "Tickets Link",
        "instructions" => "E.g. DashTickets link for online sales.",
    ],
    "facebook" => [
        "name" => "Facebook Link",
        "instructions" => "Facebook event link.",
    ],
    "description" => [
        "name" => "Description",
        "instructions" => "",
    ],
    "location" => [
        "name" => "Location",
        "instructions" => "Pin coordinates instead of an address if that is more useful.",
    ],
    "publish_date" => [
        "name" => "Publish Date",
        "instructions" => "The date at which this event will become visible on the website.",
    ],
    "student_price" => [
        "name" => "Student Price",
        "instructions" => "Only displayed if populated.",
    ],
    "nonstudent_price" => [
        "name" => "Non-Student Price",
        "instructions" => "Only displayed if populated.",
    ],
    "door_price" => [
        "name" => "Door Price",
        "instructions" => "Only displayed if populated.",
    ],

    // Meta Fields
    "meta_title" => [
        "name" => "Meta Title",
        "instructions" => "Displayed in search engines and when a link is expanded in social media posts.",
    ],
    "meta_description" => [
        "name" => "Meta Description",
        "instructions" => "Displayed in search engines and when a link is expanded in social media posts.",
    ],
    "meta_image" => [
        "name" => "Meta Image",
        "instructions" => "Displayed when a link is expanded in social media posts. See <a href=''>here</a> for a guide on creating a good meta image.",
    ],
];
