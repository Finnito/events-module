<?php namespace Finnito\EventsModule\Event;

use Finnito\EventsModule\Event\Contract\EventRepositoryInterface;
use Anomaly\Streams\Platform\Entry\EntryRepository;

/**
 * Class EventRepository
 *
 * @link          https://finnito.nz/
 * @author        Finn LeSueur <finn.lesueur@gmail.com>
 */
class EventRepository extends EntryRepository implements EventRepositoryInterface
{

    /**
     * The entry model.
     *
     * @var EventModel
     */
    protected $model;

    /**
     * Create a new EventRepository instance.
     *
     * @param EventModel $model
     */
    public function __construct(EventModel $model)
    {
        $this->model = $model;
    }


    /**
     * Find all events by a year
     *
     * @param $year
     * @return null|EventCollection
     */
    public function inYear($year)
    {
        return $this->model
            ->where("publish_date", "<=", date("Y-m-d H:i:s"))
            ->whereYear("start_time", "=", $year)
            ->orderBy("start_time", "ASC")
            ->get();
    }

    /**
     * Get a list of years in which
     * events occur.
     *
     * @return null|Collection
     */
    public function listOfYears()
    {
        return $this->model
            ->selectRaw("YEAR(start_time) as year")
            ->where("publish_date", "<=", date("Y-m-d H:i:s"))
            ->groupBy("year")
            ->orderBy("year", "DESC")
            ->get()
            ->pluck("year");
    }

    /**
     * Find an event by its type and year
     *
     * @param $type
     * @param $year
     * @return null|EventInterface
     */
    public function thisYearInType($type, $year)
    {
        return $this->model
            ->with("type")
            ->join("events_event_types", "events_events.type_id", "=", "events_event_types.id")
            ->where("events_events.publish_date", "<=", date("Y-m-d H:i:s"))
            ->whereYear("events_events.start_time", "=", $year)
            ->where("events_event_types.slug", "=", $type)
            ->orderBy("events_events.name", "ASC")
            ->get(["events_events.*"])
            ->first();
    }

    /**
     * Find an event by it's type and date
     *
     * @param $type
     * @param $date
     * @return null|EventInterface
     */
    public function eventByTypeAndDate($type, $date)
    {
        return $this->model
            ->with("type")
            ->join("events_event_types", "events_events.type_id", "=", "events_event_types.id")
            ->where("events_event_types.slug", "=", $type)
            ->whereDate("start_time", $date)
            ->orderBy("events_events.name")
            ->select("events_events.*")
            ->first();
    }

    // Put used ones above

    // public function upcomingCurrentYear($year) {
    //     return $this->model
    //         ->whereRaw("YEAR(start_time) = {$year}")
    //         ->where("start_time", ">=", date("Y-m-d H:i:s"))
    //         ->orderBy("start_time", "ASC")
    //         ->get();
    // }

    // public function pastCurrentYear($year) {
    //     return $this->model
    //         ->whereRaw("YEAR(start_time) = {$year}")
    //         ->where("start_time", "<=", date("Y-m-d H:i:s"))
    //         ->orderBy("start_time", "ASC")
    //         ->get();
    // }

    // public function mostRecentByType($type) {
    //     return $this->model
    //         ->with("type")
    //         ->join("events_event_types", "events_events.type_id", "=", "events_event_types.id")
    //         ->where("events_event_types.slug", "=", $type)
    //         ->where("events_events.publish_date", "<=", date("Y-m-d H:i:s"))
    //         ->orderBy("events_events.start_time", "DESC")
    //         ->select("events_events.*")
    //         ->first();
    // }

    // public function allByType($type) {
    //     return $this->model
    //         ->with("type")
    //         ->join("events_event_types", "events_events.type_id", "=", "events_event_types.id")
    //         ->where("events_events.publish_date", "<=", date("Y-m-d H:i:s"))
    //         ->where("events_event_types.slug", "=", $type)
    //         ->orderBy("events_events.start_time", "DESC")
    //         ->get();
    // }
}
