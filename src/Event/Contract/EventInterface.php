<?php namespace Finnito\EventsModule\Event\Contract;

use Anomaly\Streams\Platform\Entry\Contract\EntryInterface;

/**
 * Class EventInterface
 *
 * @link          https://finnito.nz/
 * @author        Finn LeSueur <finn.lesueur@gmail.com>
 */
interface EventInterface extends EntryInterface
{

}
