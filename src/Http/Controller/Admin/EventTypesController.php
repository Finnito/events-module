<?php namespace Finnito\EventsModule\Http\Controller\Admin;

use Finnito\EventsModule\EventType\Form\EventTypeFormBuilder;
use Finnito\EventsModule\EventType\Table\EventTypeTableBuilder;
use Anomaly\Streams\Platform\Http\Controller\AdminController;

/**
 * Class EventTypesController
 *
 * @link          https://finnito.nz/
 * @author        Finn LeSueur <finn.lesueur@gmail.com>
 */
class EventTypesController extends AdminController
{

    /**
     * Display an index of existing entries.
     *
     * @param EventTypeTableBuilder $table
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(EventTypeTableBuilder $table)
    {
        return $table->render();
    }

    /**
     * Create a new entry.
     *
     * @param EventTypeFormBuilder $form
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function create(EventTypeFormBuilder $form)
    {
        return $form->render();
    }

    /**
     * Edit an existing entry.
     *
     * @param EventTypeFormBuilder $form
     * @param        $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function edit(EventTypeFormBuilder $form, $id)
    {
        return $form->render($id);
    }
}
