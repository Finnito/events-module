<?php namespace Finnito\EventsModule\Http\Controller\Admin;

use Anomaly\Streams\Platform\Http\Controller\AdminController;
use Anomaly\SettingsModule\Setting\Form\SettingFormBuilder;

/**
 * Class ConfigurationController
 *
 * @link          https://finnito.nz/
 * @author        Finn LeSueur <finn.lesueur@gmail.com>
 */
class ConfigurationController extends AdminController
{

    /**
     * Edit the settings for the module
     *
     * @param SettingFormBuilder $settings
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(SettingFormBuilder $settings)
    {
        return $settings->render("finnito.module.events");
    }
}
