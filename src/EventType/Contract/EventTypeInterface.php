<?php namespace Finnito\EventsModule\EventType\Contract;

use Anomaly\Streams\Platform\Entry\Contract\EntryInterface;

/**
 * Class EventTypeInterface
 *
 * @link          https://finnito.nz/
 * @author        Finn LeSueur <finn.lesueur@gmail.com>
 */
interface EventTypeInterface extends EntryInterface
{

}
