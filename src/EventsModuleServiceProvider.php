<?php namespace Finnito\EventsModule;

use Anomaly\Streams\Platform\Addon\AddonServiceProvider;
use Finnito\EventsModule\Event\Contract\EventRepositoryInterface;
use Finnito\EventsModule\Event\EventRepository;
use Anomaly\Streams\Platform\Model\Events\EventsEventsEntryModel;
use Finnito\EventsModule\Event\EventModel;
use Finnito\EventsModule\EventType\Contract\EventTypeRepositoryInterface;
use Finnito\EventsModule\EventType\EventTypeRepository;
use Anomaly\Streams\Platform\Model\Events\EventsEventTypesEntryModel;
use Finnito\EventsModule\EventType\EventTypeModel;
use Illuminate\Routing\Router;

/**
 * Class EventsModuleServiceProvider
 *
 * @link          https://finnito.nz/
 * @author        Finn LeSueur <finn.lesueur@gmail.com>
 */
class EventsModuleServiceProvider extends AddonServiceProvider
{

    /**
     * Additional addon plugins.
     *
     * @type array|null
     */
    protected $plugins = [];

    /**
     * The addon Artisan commands.
     *
     * @type array|null
     */
    protected $commands = [];

    /**
     * The addon's scheduled commands.
     *
     * @type array|null
     */
    protected $schedules = [];

    /**
     * The addon API routes.
     *
     * @type array|null
     */
    protected $api = [];

    /**
     * The addon routes.
     *
     * @type array|null
     */
    protected $routes = [
        'admin/events'           => 'Finnito\EventsModule\Http\Controller\Admin\EventsController@index',
        'admin/events/create'    => 'Finnito\EventsModule\Http\Controller\Admin\EventsController@create',
        'admin/events/edit/{id}' => 'Finnito\EventsModule\Http\Controller\Admin\EventsController@edit',
        'admin/events/event_types'           => 'Finnito\EventsModule\Http\Controller\Admin\EventTypesController@index',
        'admin/events/event_types/create'    => 'Finnito\EventsModule\Http\Controller\Admin\EventTypesController@create',
        'admin/events/event_types/edit/{id}' => 'Finnito\EventsModule\Http\Controller\Admin\EventTypesController@edit',

        'admin/events/configuration' => 'Finnito\EventsModule\Http\Controller\Admin\ConfigurationController@index',

        "events" => "Finnito\EventsModule\Http\Controller\EventsController@index",
        "events/{type}" => "Finnito\EventsModule\Http\Controller\EventsController@type",
        "events/year/{year}" => "Finnito\EventsModule\Http\Controller\EventsController@year",
        "events/{type}/{date}" => "Finnito\EventsModule\Http\Controller\EventsController@single",
    ];

    /**
     * The addon middleware.
     *
     * @type array|null
     */
    protected $middleware = [
        //Finnito\EventsModule\Http\Middleware\ExampleMiddleware::class
    ];

    /**
     * Addon group middleware.
     *
     * @var array
     */
    protected $groupMiddleware = [
        //'web' => [
        //    Finnito\EventsModule\Http\Middleware\ExampleMiddleware::class,
        //],
    ];

    /**
     * Addon route middleware.
     *
     * @type array|null
     */
    protected $routeMiddleware = [];

    /**
     * The addon event listeners.
     *
     * @type array|null
     */
    protected $listeners = [
        //Finnito\EventsModule\Event\ExampleEvent::class => [
        //    Finnito\EventsModule\Listener\ExampleListener::class,
        //],
    ];

    /**
     * The addon alias bindings.
     *
     * @type array|null
     */
    protected $aliases = [
        //'Example' => Finnito\EventsModule\Example::class
    ];

    /**
     * The addon class bindings.
     *
     * @type array|null
     */
    protected $bindings = [
        EventsEventsEntryModel::class => EventModel::class,
        EventsEventTypesEntryModel::class => EventTypeModel::class,
    ];

    /**
     * The addon singleton bindings.
     *
     * @type array|null
     */
    protected $singletons = [
        EventRepositoryInterface::class => EventRepository::class,
        EventTypeRepositoryInterface::class => EventTypeRepository::class,
    ];

    /**
     * Additional service providers.
     *
     * @type array|null
     */
    protected $providers = [
        //\ExamplePackage\Provider\ExampleProvider::class
    ];

    /**
     * The addon view overrides.
     *
     * @type array|null
     */
    protected $overrides = [
        //'streams::errors/404' => 'module::errors/404',
        //'streams::errors/500' => 'module::errors/500',
    ];

    /**
     * The addon mobile-only view overrides.
     *
     * @type array|null
     */
    protected $mobile = [
        //'streams::errors/404' => 'module::mobile/errors/404',
        //'streams::errors/500' => 'module::mobile/errors/500',
    ];

    /**
     * Register the addon.
     */
    public function register()
    {
        // Run extra pre-boot registration logic here.
        // Use method injection or commands to bring in services.
    }

    /**
     * Boot the addon.
     */
    public function boot()
    {
        // Run extra post-boot registration logic here.
        // Use method injection or commands to bring in services.
    }

    /**
     * Map additional addon routes.
     *
     * @param Router $router
     */
    public function map(Router $router)
    {
        // Register dynamic routes here for example.
        // Use method injection or commands to bring in services.
    }
}
