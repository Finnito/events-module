<?php

use Anomaly\Streams\Platform\Database\Migration\Migration;
use Finnito\EventsModule\EventType\EventTypeModel;

class FinnitoModuleEventsCreateEventsFields extends Migration
{

    /**
     * The addon fields.
     *
     * @var array
     */
    protected $fields = [
        // Shared Fields
        "name" => "anomaly.field_type.text",
        "slug" => [
            "type" => "anomaly.field_type.slug",
            "config" => [
                "slugify" => "name",
                "type" => "-",
            ],
        ],

        // SEO Fields
        "meta_title" => "anomaly.field_type.text",
        "meta_description" => [
            "type" => "anomaly.field_type.textarea",
            "config" => [
                "show_counter" => true,
                "max" => 300,
            ],
        ],
        "meta_image" => [
            "type" => "anomaly.field_type.image",
            "config" => [
                "folders" => [
                    "events",
                ],
            ],
        ],

        // Event Specific Fields
        "type" => [
            "type" => "anomaly.field_type.relationship",
            "config" => [
                "related" => EventTypeModel::class,
                "mode" => "search",
            ],
        ],
        "banner" => [
            "type" => "anomaly.field_type.image",
            "config" => [
                "folders" => [
                    "events",
                ],
            ],
        ],
        "description" => [
            "type" => "anomaly.field_type.wysiwyg",
        ],
        "start_time" => [
            "type" => "anomaly.field_type.datetime",
        ],
        "end_time" => [
            "type" => "anomaly.field_type.datetime",
        ],
        "publish_date" => [
            "type" => "anomaly.field_type.datetime",
        ],
        "student_price" => [
            "type" => "anomaly.field_type.integer",
            "config" => [
                "separator" => "",
            ],
        ],
        "nonstudent_price" => [
            "type" => "anomaly.field_type.integer",
            "config" => [
                "separator" => "",
            ],
        ],
        "door_price" => [
            "type" => "anomaly.field_type.integer",
            "config" => [
                "separator" => "",
            ],
        ],
        "location" => [
            "type" => "anomaly.field_type.geocoder",
        ],
        "logo" => [
            "type" => "anomaly.field_type.image",
            "config" => [
                "folders" => "events",
            ],
        ],
        "tickets" => [
            "type" => "anomaly.field_type.url",
        ],
        "facebook" => [
            "type" => "anomaly.field_type.url",
        ],
    ];

}
